Bug-Database: https://github.com/ming1/ubdsrv/issues
Bug-Submit: https://github.com/ming1/ubdsrv/issues/new
Changelog: https://github.com/ming1/ubdsrv/commits/master
Documentation: https://github.com/ming1/ubdsrv/blob/master/README.rst
Repository-Browse: https://github.com/ming1/ubdsrv/
Repository: https://github.com/ming1/ubdsrv.git
