#!/bin/bash

set -e

load_driver(){
    modprobe ublk_drv
    if [ $? -eq 0 ]; then
	echo "ublk_drv module loaded successfully"
    else
	echo "Failed to load ublk_drv module"
    fi
}

test_generic_001() {
    TEST_DIR=/tmp
    UBLK=/usr/sbin/ublk
    ./tests/run_test.sh generic/001 1 /tmp
}

test_generic_002() {
    TEST_DIR=/tmp
    UBLK=/usr/sbin/ublk
    ./tests/run_test.sh generic/002 1 /tmp
}

test_generic_003() {
    TEST_DIR=/tmp
    UBLK=/usr/sbin/ublk
    ./tests/run_test.sh generic/003 1 /tmp
}

load_driver
test_generic_001
test_generic_002
test_generic_003
